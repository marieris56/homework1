fun addNumbers(X: Int, Y: Int):Int
{
    return X+Y
}
fun main()
{
    var calc = true
    while (calc){
        println("შეიყვანეთ რიცხვი")
        val X = Integer.valueOf(readLine())
        println("შეიყვანეთ მეორე რიცხვი")
        val Y = Integer.valueOf(readLine())
        val Z = addNumbers(X,Y)
        println("X და Y ჯამი არის: $Z")
        println("გსურთ პროგრამის ხელახლა დაწყება? :")
        val answer = readLine()
        if(answer == "არა"){
            println("დასასრული")
            calc = false
        } else if (answer == "დიახ") {
            println("იწყება ხელახლა")
            continue
        } else {
            println("შეიყვანეთ მხოლოდ დიახ ან არა!")
            val answer = readLine()
        }
    }
}